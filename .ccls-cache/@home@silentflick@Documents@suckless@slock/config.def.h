/* user and group to drop privileges to */
static const char *user = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
    [INIT] = "black",     /* after initialization */
    [INPUT] = "#2E3440",  /* during input */
    [FAILED] = "#CC3333", /* wrong password */
    [CAPS] = "#551A8B",   /* CapsLock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* default message */
static const char *message = "Do you know how to use this computer?";

/* text color */
static const char *text_color = "#ffffff";

/* text size (must be a valid size) */
static const char *font_name =
    "-xos4-terminus-medium-r-normal--16-160-72-72-c-80-iso10646-1";

/* allow control key to trigger fail on clear */
static const int controlkeyclear = 0;
